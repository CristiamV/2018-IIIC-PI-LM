/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriodos;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private String nombre;
    private double tParedes;
    private double tVentanas;

    private final int MIN_X_M2 = 10;
    private final int PRE_X_HR = 30;

    public Logica(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Agrega una pared al total 
     * @param largo double largo de la pared
     * @param ancho double ancho de la pared
     */
    public void agregarPared(double largo, double ancho) {
        Rectangulo pared = new Rectangulo(largo, ancho);
        tParedes += pared.calcularArea();
    }

    /**
     * Agrega una ventana rectangular al total de ventanas 
     * @param largo double largo de la ventana
     * @param ancho  double ancho de la ventana 
     */
    public void agregarVentana(double largo, double ancho) {
        Rectangulo ventana = new Rectangulo(largo, ancho);
        tVentanas += ventana.calcularArea();
    }

    /**
     * Agrega una ventana circular al total del area de ventanas
     * @param ancho double ancho de la ventana
     */
    public void agregarVentana(double ancho) {
        Circunferencia ventana = new Circunferencia(ancho / 2);
        tVentanas += ventana.calcularArea();
    }

    /**
     * Genera un reporte con los datos de la cotización
     * @return string reporte 
     */
    public String cotizar() {
        String resutaldos = "Cliente: %s\n\n"
                + "Área Paredes:  %.2f\n"
                + "Área Ventanas: %.2f\n"
                + "Área Pintar:   %.2f\n\n"
                + "Tiempo: %d días y %d horas\n"
                + "Costo: $%d";
        double aPintar = tParedes - tVentanas;
        int tHrs = (int) Math.round((aPintar * MIN_X_M2) / 60 + 0.5);
        int costo = tHrs * PRE_X_HR;
        return String.format(resutaldos, nombre, tParedes,
                tVentanas, aPintar, (tHrs / 24), (tHrs % 24), costo);

    }

}
