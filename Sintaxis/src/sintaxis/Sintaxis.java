/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class Sintaxis {

    static int x;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        Logica log = new Logica();
//        int a = Integer.parseInt(JOptionPane.showInputDialog("#1: "));
//        int b = Integer.parseInt(JOptionPane.showInputDialog("#2: "));
//
//        System.out.println(String.format("%d+%d=%d", a, b, log.sumar(a, b)));
//        System.out.println(String.format("%d-%d=%d", a, b, log.restar(a, b)));
//        System.out.println(String.format("%d*%d=%d", a, b, log.multiplicar(a, b)));
//        System.out.println(String.format("%d/%d=%d", a, b, log.dividir(a, b)));

        // TODO code application logic here
//        System.out.println("Hola Mundo!!");
//        System.out.println("Hola Mundo!! xD");
//        System.out.println(Sintaxis.x);
//        int x = 10;
//        System.out.println(x);
//
//        boolean asd = false;
//        int numE = 121312123;
//        long numL = 1213123123123L;
//        float num = 123.23F;
//        double num2 = 123.33;
//
//        char carac1 = 'c';
//        char carac2 = '*';
//        char carac3 = '2';
//        char carac4 = ' ';
//
//        //Caracteres de escape
//        System.out.println("Hola\tMundo");
//        System.out.println("Hola\nMundo");
//        System.out.println("Hola\\Mundo");
//        System.out.println("\"Hola Mundo\"");
//        System.out.println("\u2211");
//
//        String nombre = "Allan Murillo A";
//
//        System.out.println("A\tB\tC\n1\t2\t3\n");
//        System.out.println("\"HOLA\"");
//        System.out.println("El caracter de escape \\n cambia de línea ");
//        System.out.println("El unicode \\u0040 vale \u0040");
//        System.out.println("El unicode \\u00D1 vale \u00D1");
//
//        //Variables
//        //Declarar variables 
//        int numero;
//        //Inicialización
//        numero = 120;
//        System.out.println(numero);
//        //Asignación
//        numero = 10;
//        System.out.println(numero);
//
//        int as = 12;
//        //Consatante
//        final String RUTA = "C:\\Users\\ALLAN\\Documents\\Proyectos";
//        System.out.println(RUTA);
//
//        boolean isReal = true;
//        byte b = 122;
//        short s = -2900;
//        long l = 999999999999L;
//        float f1 = 234.99F;
//        double d;
//        char cvalue = '4';
//        System.out.println(cvalue);
//        final double PI = 3.1415926;
//        int variable1 = (int) 32.99;
//        double variable2 = (double) 3;
//        System.out.println(variable2);
//        System.out.println(Math.abs(123));
//        System.out.println(35 / 4.0);
//
//        double div = 34 / 8.0;
//        System.out.println(div);
//
//        //Operdares Unarios
//        int c = 10;
//        c++;
////        Alternativas
////        c = c + 1;
////        c += 1;
//
//        int y = ++c;
//        System.out.println(y);
//        System.out.println(c);
//
//        //Asignación 
//        c = 10;
//        c += 12;
//
////        String nom = JOptionPane.showInputDialog("Nombre: ");
////        String ape1 = JOptionPane.showInputDialog("Primer Apellido: ");
////        String ape2 = JOptionPane.showInputDialog("Segundo Apellido: ");
////        String nc = nom + " " + ape1 + " " + ape2;
////        System.out.println(nc);
//        String menu = "Menu Principal\n"
//                + " 1. Nombre\n"
//                + " 2. Apellidos\n"
//                + " 3. Imprimir\n"
//                + "Digite una opción: ";
//        String op = JOptionPane.showInputDialog(menu);
//
//        JOptionPane.showMessageDialog(null, "Lo que quiero mostrar",
//                "Título de la ventana", JOptionPane.INFORMATION_MESSAGE);
//        JOptionPane.showMessageDialog(null, "Lo que quiero mostrar",
//                "Título de la ventana", JOptionPane.QUESTION_MESSAGE);
//        JOptionPane.showMessageDialog(null, "Lo que quiero mostrar",
//                "Título de la ventana", JOptionPane.ERROR_MESSAGE);
//        JOptionPane.showMessageDialog(null, "Lo que quiero mostrar",
//                "Título de la ventana", JOptionPane.WARNING_MESSAGE);
//        JOptionPane.showMessageDialog(null, "Lo que quiero mostrar",
//                "Título de la ventana", JOptionPane.PLAIN_MESSAGE);
//
//        String asdas = null;
//        System.out.println(asdas);
        Logica log = new Logica();
        Casa c1 = new Casa("Azul", "SC");
        int canCas = 1;
        //Paso por valor para tipos primitivos
        System.out.println(canCas);
        canCas = log.incrementarCasas(canCas);
        System.out.println(canCas);
        
        //Paso de parametro por referencia
        System.out.println(c1.color);
        log.modificarCasa(c1);
        System.out.println(c1.color);
       
        
         
        
    }

}
