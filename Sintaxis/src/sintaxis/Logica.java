/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

/**
 *
 * @author ALLAN
 */
public class Logica {
    //nivel acceso + tipo* + nombre + (parametros*)

    /**
     * Suma dos números enteros
     *
     * @param a Primer número entero
     * @param b Segundo número entero
     * @return El resultado de sumar a y b
     */
    public static int sumar(int a, int b) {
        return a + b;
    }

    /**
     * Resta dos números enteros
     *
     * @param a Primer número entero
     * @param b Segundo número entero
     * @return El resultado de restar a y b
     */
    public int restar(int a, int b) {
        return a - b;
    }

    /**
     * Multiplica dos números enteros
     *
     * @param a Primer número entero
     * @param b Segundo número entero
     * @return El resultado de multiplicar a y b
     */
    public int multiplicar(int a, int b) {
        return a * b;
    }

    /**
     * Divide dos números enteros
     *
     * @param a Primer número entero
     * @param b Segundo número entero
     * @return El resultado de dividir a y b
     */
    public int dividir(int a, int b) {
        return a / b;
    }

    public int incrementarCasas(int x) {
        x = x + 1;
        return x;
    }

    public void modificarCasa(Casa ca) {
        ca.color = "Amarillo";
    }
    
}
