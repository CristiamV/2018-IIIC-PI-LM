/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareas;

/**
 *
 * @author ALLAN
 */
public class OtroNumero {

    private int numero;

    public OtroNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Determina si un número es primo(true) o si es compuesto(false)
     *
     * @return true cuando el número es primo
     */
    public boolean esPrimo() {
        int div = 0;
        for (int i = 1; i <= numero; i++) {
            if (numero % i == 0) {
                div++;
            }
            if (div > 2) {
                return false;
            }
        }
        return div == 2;
    }

    /**
     * Determina si el número es perfecto, abundante o deficiente
     *
     * @return int 1 cuando es perfecto, 2 deficiente o 3 cuando es abundante
     */
    public int tipoNumero() {

        int sumDiv = 0;

        for (int i = 1; i < numero; i++) {
            if (numero % i == 0) {
                sumDiv += i;
            }
        }

        return sumDiv < numero ? 2 : sumDiv > numero ? 3 : 1;
    }

    //Getters & Setters
    
    public int getNumero() {
        return numero;
    }

}
