/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class Principal {

    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        int num = Integer.parseInt(sc.nextLine());
        return num;
    }

    public static char leerChar(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        char letra = sc.nextLine().charAt(0);
        return letra;
    }

    public static void main(String[] args) {
        Calculadora calc = new Calculadora();
        String menu = "Calculadora UTN v0.1\n"
                + "(+) Sumar\n"
                + "(-) Restar\n"
                + "(*) Multiplicar\n"
                + "(/) Dividir\n"
                + "(s) Salir\n"
                + "Seleccione una opción";
        boolean salir = false;
        APP:
        do{
            char op = leerChar(menu);
            if (op == 's') {
                break;
            }
            int num1 = leerInt("#1");
            int num2 = leerInt("#2");
            switch (op) {
                case '+':
                    String res = String.format("%d+%d=%d", num1, num2, calc.sumar(num1, num2));
                    System.out.println(res);
                    break;
                case '-':
                    res = String.format("%d-%d=%d", num1, num2, calc.restar(num1, num2));
                    System.out.println(res);
                    break;
                case '*':
                    res = String.format("%d*%d=%d", num1, num2, calc.multiplicar(num1, num2));
                    System.out.println(res);
                    break;
                case '/':
                    res = String.format("%d/%d=%.2f", num1, num2, calc.dividir(num1, num2));
                    System.out.println(res);
                    break;
//                case 's':
//                    salir = true; //Alternivo, para no usar while(true)
//                    System.out.println("Gracias por utilizar la aplicación");
//                    break APP;
                default:
                    System.out.println("Opción Inválida");
            }

        } while (!salir);        
        
    }
}
