/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class Principal {

    public static void main(String[] args) {
//        Logica log = new Logica();
//        int aleatorio = log.generar();//Generar
//        System.out.println(aleatorio);
//
//        for (int i = 1; i <= 5; i++) {
//            //Pedir el número
//            int num = Util.leerInt("Digite un número");
//            //Gano
//            if (log.gano(aleatorio, num)) {
//                System.out.println("Adivinaste");
//                break;
//            }
//            //Perdio
//            if (log.perdio(i)) {
//                System.out.println("Perdió se acabaron sus intentos");
//                break;
//            }
//            //Mayor, Menor
//            System.out.println(log.pista(num, aleatorio));
//        }
        String menu = "Dificultad\n"
                + "1. Fácil(1-10)\n"
                + "2. Medio(1-50)\n"
                + "3. Difícil(1-100)\n"
                + "4. Nivel Dios(1-1M)\n"
                + "Selecciones una opción";

        int dif = Util.leerInt(menu);
        Juego j = new Juego(dif);
        System.out.println(j.getAleatorio());
        while (true) {
            System.out.println("Tiene " + j.getIntentos() + " intentos");
            int num = Util.leerInt("Digite un número");
            j.jugar(num);
            if (j.isGano()) {
                System.out.println("Ganó");
                break;
            } else if (j.getIntentos() == 0) {
                System.out.println("Perdió");
                System.out.println("El número era: " + j.getAleatorio());
                break;
            }
            System.out.println(j.pista(num));
        }
    }
}
