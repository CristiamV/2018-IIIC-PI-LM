/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

/**
 *
 * @author ALLAN
 */
public class Juego {

    private int intentos;
    private int aleatorio;
    private boolean gano;

    public Juego(int dificultad) {
        this.intentos = 5;
        aleatorio = generar(dificultad);
    }

    public void jugar(int num) {
        intentos--;
        if (num == aleatorio) {
            gano = true;
        }
    }

    public String pista(int num) {
        String res = num < aleatorio ? "Es mayor" : "Es menor";
        return res;
    }

    public int getIntentos() {
        return intentos;
    }

    public boolean isGano() {
        return gano;
    }

    private int generar(int dif) {
        int limMax = 10;
        switch (dif) {
            case 2:
                limMax = 50;
                break;
            case 3:
                limMax = 100;
                break;
            case 4:
                limMax = 1000000;
                break;
        }
        return (int) (Math.random() * limMax) + 1;
    }

    public int getAleatorio() {
        return intentos == 0 ? aleatorio : -1;
    }

}
