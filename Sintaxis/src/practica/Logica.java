/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

/**
 *
 * @author ALLAN
 */
public class Logica {

    /**
     * Obtiene los multiplos de un número entre 1 y 1000
     *
     * @param num int para indicar los multiplos del num
     * @return String con los multiplos de num
     */
    public String multiplos(int num) {
        String res = "";
        for (int i = 1; i < 1000; i++) {
            if (i % num == 0) {
                res += i + "\n";
            }
        }
        return res;
    }

    public int generar() {
        return (int) (Math.random() * 10) + 1;
    }

    public boolean gano(int aleatorio, int num) {
        return aleatorio == num;
    }

    public boolean perdio(int i) {
        return i == 5;
    }

    public String pista(int num, int aleatorio) {
        if (num > aleatorio) {
            return "El número tiene que ser menor";
        } else {
            return "El número tiene que ser mayor";

        }
    }

}
